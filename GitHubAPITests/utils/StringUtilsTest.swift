//
//  StringUtilsTest.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 03/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import XCTest

@testable import GitHubAPI

class StringUtilsTest: XCTestCase {
    
    override func setUp() {
        
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSetUpOpenMessage() {
        let stringTest = StringHelper.setupOpenMessage(closed: 2, open: 3);
        XCTAssertEqual(stringTest, "3 opened / 2 closed");
    }
    
    func testSetUpOpenMessage2() {
        let stringTest = StringHelper.setupOpenMessage(closed: 5, open: 1);
        XCTAssertEqual(stringTest, "1 opened / 5 closed");
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
    
}
