//
//  RepositorySpec.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//
@import Foundation;

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>

#import "Repository.h"

SpecBegin(Repository)

describe(@"Repository", ^{
    __block Repository *repository;
    __block NSDictionary *dictionary;
    beforeAll(^{
        
        dictionary = @{
                      @"id_n" : @1234,
                      @"name" : @"teste",
                      @"descript" : @"testeteste123",
                      @"forks" : @1234,
                      @"stargazers_count" : @1234,
                      @"full_name" : @"testeteste",
                      @"owner" : @"testeowner",
                      @"pulls_url" : @"testeurl"};
       
    });
    
    describe(@"Should implement test methods", ^{
        it(@"should test init repository", ^{
            NSError *error;
            repository = [Repository new];
            repository = [repository initWithDictionary:dictionary error:&error];
            expect(repository).notTo.beNil();
            expect(repository.id_n).equal(dictionary[@"id_n"]);
            expect(repository.descript).equal(dictionary[@"descript"]);
            expect(repository.name).equal(dictionary[@"name"]);
            expect(repository.forks).equal(dictionary[@"forks"]);
            expect(repository.stargazers_count).equal(dictionary[@"stargazers_count"]);
            expect(repository.full_name).equal(dictionary[@"full_name"]);
            expect(repository.owner).equal(dictionary[@"owner"]);
        });
    
    });
    
    afterAll(^{
        dictionary = nil;
        repository = nil;
    });
});

SpecEnd
