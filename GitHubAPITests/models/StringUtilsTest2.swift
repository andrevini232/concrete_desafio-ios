//
//  PullRequest.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 03/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import Quick
import Nimble
@testable import GitHubAPI

class StringUtilsTest2 : QuickSpec{
    override func spec(){
        
        var message : String?
        beforeEach {
            message = StringHelper.setupOpenMessage(closed: 5, open: 4)
        }
        describe("Should test PullRequest model"){
            it("first test"){
                expect(message).to(equal("4 opened / 5 closed"));
            }
        }
    }
}
