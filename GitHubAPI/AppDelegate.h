//
//  AppDelegate.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//
@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

