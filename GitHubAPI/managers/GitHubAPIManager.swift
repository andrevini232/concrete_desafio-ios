//
//  GitHubAPIManager.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 03/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import UIKit

public class GitHubAPIManager: NSObject {
    
    var business : GitHuBusiness?
    
    @objc static let sharedInstance = GitHubAPIManager()
    private override init() {
        self.business = GitHuBusiness()
    }
    
    func fetchRepositories(stringURL : String, failure fail : ((NSError) -> ())? = nil,
        success succeed: ((RepositoryListModel) -> ())? = nil){
        self.business?.fetchMostTopJavaRepositories(stringURL, success: { (responseList) in
            succeed!(responseList!)
        }, failure: { (error) in
            
        })
    }
    
    func fetchPullRequests(repository: Repository, failure fail : ((NSError) -> ())? = nil,
                           success succeed: ((Repository) -> ())? = nil){
        self.business?.fetchRepositoryPullRequests(repository, success: { (repository) in
            succeed!(repository!)
        }, failure: { (error) in
            
        })
    }
    
    
}
