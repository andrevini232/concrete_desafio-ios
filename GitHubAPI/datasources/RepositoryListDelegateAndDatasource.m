//
//  RepositoryListDelegateAndDatasource.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "RepositoryListDelegateAndDatasource.h"
#import "GitHubAPI-Swift.h"

static NSString * const list1 = @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";

@interface RepositoryListDelegateAndDatasource () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) RepositoryListModel *listModel;
@property (nonatomic) id<RepositoryListDelegate> delegate;
@property (nonatomic) id<RepositoryPullRequestDelegate> delegateToPullRequest;
@end

@implementation RepositoryListDelegateAndDatasource

#pragma mark - Public methods
- (void)initRepositoryListDataSourceWithDelegate:(id<RepositoryListDelegate>)delegate delegatePullRequest:(id<RepositoryPullRequestDelegate>)delegatePullRequest{
    self.delegate = delegate;
    self.delegateToPullRequest = delegatePullRequest;
    GitHubAPIManager *manager = [GitHubAPIManager sharedInstance];
    [manager fetchRepositoriesWithStringURL:list1 failure:^(NSError * error) {
        
    } success:^(RepositoryListModel * responseModel) {
        self.listModel = responseModel;
        [self fetchDidComplete];
    }];
}

- (void)fetchDidComplete{
    if([self.delegate conformsToProtocol:@protocol(RepositoryListDelegate)]){
        [self.delegate reloadFetchDataWithRepository];
    }
}

- (void)pushPullRequestViewController:(Repository *)repository{
    if([self.delegateToPullRequest conformsToProtocol:@protocol(RepositoryPullRequestDelegate)]){
        [self.delegateToPullRequest pushPullRequestViewController:repository];
    }
}


@end

@implementation RepositoryListDelegateAndDatasource (UITableViewDataSource)

#pragma mark - UITableViewDatasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.listModel != nil){
        return self.listModel.items.count;
    }
    return 1;
}

- (RepositoryCellTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RepositoryCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRepositoryCellID];
    
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:kRepositoryCellID bundle:nil] forCellReuseIdentifier:kRepositoryCellID];
        cell = [tableView dequeueReusableCellWithIdentifier:kRepositoryCellID];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(RepositoryCellTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.listModel.items != nil){
        
        Repository *repository = [self.listModel.items objectAtIndex:indexPath.row];
        cell.repositoryName.text = repository.name;
        cell.descriptRepository.text = repository.descript;
        // cell.userFullName.text = repository.owner.;
        cell.userName.text = repository.owner.login;
        cell.forks.text = [repository.forks stringValue];
        cell.stars.text = [repository.stargazers_count stringValue];
        
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:repository.owner.avatar_url]];
        
        [cell.userImageView setClipsToBounds:YES];
        [cell.userImageView layoutIfNeeded];
        cell.userImageView.layer.cornerRadius = 2.0;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RepositoryCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepositoryCellTableViewCell"];
    Repository *repository = self.listModel.items[indexPath.row];
    [self pushPullRequestViewController:repository];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    RepositoryHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:@"RepositoryHeaderView" owner:self options:nil] objectAtIndex:0];;
    
    return view;
}


@end
