//
//  RepositoryListDelegateAndDatasource.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

@import UIKit;
@import Foundation;
@import SDWebImage;

#import "RepositoryCellTableViewCell.h"
#import "GitHuBusiness.h"
#import "RepositoryListDelegate.h"
#import "PullRequestViewController.h"
#import "RepositoryPullRequestDelegate.h"


@interface RepositoryListDelegateAndDatasource : NSObject

- (void)initRepositoryListDataSourceWithDelegate:(id<RepositoryListDelegate>)delegate delegatePullRequest:(id<RepositoryPullRequestDelegate>)delegateRepository;

@end
