//
//  PullRequestDelegateANdDataSource.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

#import "PullRequestDelegateAndDataSource.h"
#import "GitHubAPI-Swift.h"

@interface PullRequestDelegateAndDataSource ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) Repository *repository;
@property (nonatomic) NSArray<PullRequest *> *pullsArray;
@property (nonatomic) id<RepositoryPullRequestDelegate> delegate;
@end

@implementation PullRequestDelegateAndDataSource

- (void)initPullRequestDataSourceWithRepository:(Repository *)repository delegate:(id<RepositoryPullRequestDelegate>)delegate{
    self.delegate = delegate;
    self.repository = repository;
    GitHubAPIManager *manager = [GitHubAPIManager sharedInstance];
    [manager fetchPullRequestsWithRepository:repository failure:^(NSError * error) {
        
    } success:^(Repository * repository) {
        self.repository = repository;
        [self fetchDidComplete];
        NSArray *openAndClosedNumbers = [self setupOpenClosedNumbers];
        NSString *openMessage = [StringHelper setupOpenMessageWithClosed:[openAndClosedNumbers objectAtIndex:1] open:[openAndClosedNumbers objectAtIndex:0]];
        [self setHeaderFooterMessage:openMessage];
    }];
}

- (void)setHeaderFooterMessage:(NSString *)message{
    if([self.delegate conformsToProtocol:@protocol(RepositoryPullRequestDelegate)]){
        [self.delegate setHeaderFooterMessage:message];
    }

}

- (NSArray *)setupOpenClosedNumbers{
    int closed = 0;
    int open = 0;
    NSArray *arrayNumbers;
    for (PullRequest *pull in _repository.pullrequests) {
        if([pull.state isEqualToString:@"closed"]){
            closed +=1;
        }else if([pull.state isEqualToString:@"open"]){
            open +=1;
        }
    }
   
    NSNumber *openNumber = [NSNumber numberWithInt:open];
    NSNumber *closedNumber = [NSNumber numberWithInt:closed];
    arrayNumbers = @[openNumber, closedNumber];
    
    return arrayNumbers;
}
- (void)fetchDidComplete{
    if([self.delegate conformsToProtocol:@protocol(RepositoryPullRequestDelegate)]){
        [self.delegate reloadFetchDataWithPullRequests];
    }
}

@end

@implementation PullRequestDelegateAndDataSource(UITableViewDataSource)

- (PullRequestCellTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PullRequestCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPullRequestCellID];
    
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:kPullRequestCellID bundle:nil] forCellReuseIdentifier:kPullRequestCellID];
        cell = [tableView dequeueReusableCellWithIdentifier:kPullRequestCellID];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(PullRequestCellTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.repository.pullrequests.count > 0){
        PullRequest *request = [self.repository.pullrequests objectAtIndex:indexPath.row];
        cell.userLogin.text = request.user.login;
        cell.pullRequestTitle.text = request.title;
        cell.descriptPullRequest.text = request.body;
        
        
        [cell.userImage sd_setImageWithURL:[NSURL URLWithString:request.user.avatar_url]];
        cell.datapullRequest.text = request.created_at_date.description;
    
        [cell.userImage setClipsToBounds:YES];
        [cell.userImage layoutIfNeeded];
        cell.userImage.layer.cornerRadius = 2.0;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.repository.pullrequests.count > 0){
        return self.repository.pullrequests.count;
    }
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 170;
}

@end
