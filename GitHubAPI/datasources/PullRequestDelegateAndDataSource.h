//
//  PullRequestDelegateANdDataSource.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

@import Foundation;
@import UIKit;
@import SDWebImage;

#import "StringUtils.h"
#import "Repository.h"
#import "GitHuBusiness.h"
#import "PullRequestCellTableViewCell.h"
#import "RepositoryPullRequestDelegate.h"


@interface PullRequestDelegateAndDataSource : NSObject

- (void)initPullRequestDataSourceWithRepository:(Repository *)repository delegate:(id<RepositoryPullRequestDelegate>)delegate;

@end
