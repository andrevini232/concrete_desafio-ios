//
//  GitHuBusiness.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "GitHuBusiness.h"

@implementation GitHuBusiness

#pragma mark - Public methods

- (void)fetchMostTopJavaRepositories:(NSString *)addressRequest
                             success:(void (^)(RepositoryListModel *responseModel))success
                             failure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:addressRequest parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSError *error = nil;
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        RepositoryListModel *list = [MTLJSONAdapter modelOfClass:RepositoryListModel.class fromJSONDictionary:responseDictionary error:&error];
        success(list);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }];
}

- (void)fetchRepositoryPullRequests:(Repository *)repository
                            success:(void (^)(Repository *repository))success
                            failure:(void (^)(NSError *error))failure{
    NSString *pullRequestsURL = repository.pulls_url;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:pullRequestsURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSError *error = nil;
        NSArray *responseArray = (NSArray *)responseObject;
 
        NSArray *objects = [MTLJSONAdapter modelsOfClass:[PullRequest class] fromJSONArray:responseArray error:nil];
        
        repository.pullrequests = objects;
        success(repository);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error);
    }
     ];
}

- (void)fetchUserWithUrl:(NSString *)userURL repositoryList:(RepositoryListModel *)repositoryList success:(void (^)(RepositoryListModel *responseList))success failure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:userURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *responseDictionary = (NSDictionary *)responseObject;
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}


@end
