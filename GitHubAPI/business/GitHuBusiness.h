//
//  GitHuBusiness.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

@import Foundation;
@import AFNetworking;

#import "Repository.h"
#import "RepositoryListModel.h"

@interface GitHuBusiness : NSObject

- (void)fetchMostTopJavaRepositories:(NSString *)addressRequest
                             success:(void (^)(RepositoryListModel *responseModel))success
                             failure:(void (^)(NSError *error))failure;
- (void)fetchRepositoryPullRequests:(Repository *)repository
                            success:(void (^)(Repository *repository))success
                            failure:(void (^)(NSError *error))failure;
- (void)fetchUserWithUrl:(NSString *)userURL repositoryList:(RepositoryListModel *)repositoryList success:(void (^)(RepositoryListModel *responseList))success failure:(void (^)(NSError *error))failure;
@end
