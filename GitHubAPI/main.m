//
//  main.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
