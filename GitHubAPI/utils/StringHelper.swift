//
//  StringHelper.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 03/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import UIKit

open class StringHelper: NSObject {

    @objc public static func setupOpenMessage(closed: NSNumber, open: NSNumber)->String{
        var openMessage = "";
        openMessage = "\(open) opened / \(closed) closed";
        return openMessage;
    }
}
