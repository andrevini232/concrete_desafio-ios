//
//  StringUtils.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 02/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

#ifndef StringUtils_h
#define StringUtils_h


#endif /* StringUtils_h */
extern NSString * const kStoryboardID;
extern NSString * const kPullRequestControllerID;
extern NSString * const kPullRequestCellID;
extern NSString * const kRepositoryCellID;

extern NSString * const kLoginKey;
extern NSString * const kLoginValue;

extern NSString * const kIDkey;
extern NSString * const kIDValue;

extern NSString * const kAvatarKey;
extern NSString * const kAvatarValue;

extern NSString * const kURLKey;
extern NSString * const kURLValue;

extern NSString * const kItemsKey;
extern NSString * const kItemsValue;
