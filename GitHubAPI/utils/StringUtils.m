//
//  StringUtils.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 02/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

@import Foundation;

NSString * const kStoryboardID = @"GithubScreens";
NSString * const kPullRequestControllerID = @"PullRequestViewController";
NSString * const kPullRequestCellID = @"PullRequestCellTableViewCell";

NSString * const kRepositoryCellID = @"RepositoryCellTableViewCell";

NSString * const kLoginKey = @"login";
NSString * const kLoginValue = @"login";

NSString * const kIDkey = @"id_n";
NSString * const kIDValue = @"id";

NSString * const kAvatarKey = @"avatar_url";
NSString * const kAvatarValue = @"avatar_url";

NSString * const kURLKey = @"url";
NSString * const kURLValue = @"value";

NSString * const kItemsKey = @"items";
NSString * const kItemsValue = @"items";
