//
//  PullRequest.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

@import Foundation;

#import "PullRequestUser.h"
#import <Mantle/Mantle.h>
#import "StringUtils.h"

@interface PullRequest : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber * id_n;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * body;
@property (nonatomic, copy) NSString * created_at;
@property (nonatomic, copy) NSString * updated_at;
@property (nonatomic, copy) NSString * state;
@property (nonatomic, copy) NSDate *created_at_date;
@property (nonatomic, copy) NSDate *updated_at_date;
@property (nonatomic, copy) PullRequestUser * user;

@end
