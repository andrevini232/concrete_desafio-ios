//
//  PullRequestUser.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "StringUtils.h"

@interface PullRequestUser : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber * id_n;
@property (nonatomic, copy) NSString * login;
@property (nonatomic, copy) NSString * avatar_url;
@property (nonatomic, copy) NSString * url;

@end
