//
//  RepositoryListModel.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 25/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//
@import Foundation;

#import <Mantle/Mantle.h>
#import "Repository.h"
#import "StringUtils.h"

@interface RepositoryListModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray *items;

@end
