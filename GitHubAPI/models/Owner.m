//
//  Owner.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "Owner.h"

@implementation Owner

#pragma mark - Mantle JSONKeyPathsByPropertyKey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             kLoginKey : kLoginValue,
             kIDkey : kIDValue,
             kAvatarKey : kAvatarValue,
             kURLKey : kURLValue,
             };
}

@end
