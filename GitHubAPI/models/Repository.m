//
//  Repository.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "Repository.h"

@implementation Repository

#pragma mark - Mantle JSONKeyPathsByPropertyKey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             kIDkey : kIDValue,
             @"name" : @"name",
             @"descript" : @"description",
             @"forks" : @"forks",
             @"stargazers_count" : @"stargazers_count",
             @"full_name" : @"full_name",
             @"owner" : @"owner",
             @"pulls_url" : @"pulls_url"
             };
}

#pragma mark - Override methods
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;

    if (self.pulls_url != nil && self.pulls_url.length > 9){
        self.pulls_url = [_pulls_url substringWithRange:NSMakeRange(0, _pulls_url.length-9)];
    }
    
    self.pullrequests = [NSMutableArray array];
    return self;
}

@end
