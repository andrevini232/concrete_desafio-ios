//
//  PullRequestUser.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "PullRequestUser.h"

@implementation PullRequestUser

#pragma mark - Mantle JSONKeyPathsByPropertyKey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             kIDkey : kIDValue,
             kLoginKey : kLoginValue,
             kAvatarKey : kAvatarValue,
             kURLKey : kURLValue,
             };
}

@end
