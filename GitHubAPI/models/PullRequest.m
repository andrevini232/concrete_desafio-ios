//
//  PullRequest.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "PullRequest.h"

@implementation PullRequest

#pragma mark - Mantle JSONKeyPathsByPropertyKey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             kIDkey : kIDValue,
             @"title" : @"title",
             @"created_at" : @"created_at",
             @"updated_at" : @"updated_at",
             @"user" : @"user",
             @"body" : @"body",
             @"state" : @"state"
             };
}

#pragma mark - Override methods
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    self.updated_at_date = [[PullRequest dateFormatter] dateFromString:self.updated_at];
    self.created_at_date = [[PullRequest dateFormatter] dateFromString:self.created_at];
    return self;
}

#pragma mark - Private methods
+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    //dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}

@end
