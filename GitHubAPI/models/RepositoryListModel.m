//
//  RepositoryListModel.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 25/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "RepositoryListModel.h"

@class Repository;

@implementation RepositoryListModel

#pragma mark - Mantle JSONKeyPathsByPropertyKey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             kItemsKey : kItemsValue,
             };
}

#pragma mark - Mantle itemsJSONTransformer
+ (NSValueTransformer *)itemsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:Repository.class];
}
@end
