//
//  Owner.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

@import Foundation;
@import UIKit;

#import "StringUtils.h"
#import <Mantle/Mantle.h>

@interface Owner : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber * id_n;
@property (nonatomic, copy) NSString * login;
@property (nonatomic, copy) NSString * avatar_url;
@property (nonatomic, copy) NSString * url;
@property (nonatomic) UIImage *imageView;

@end
