//
//  Repository.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 24/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//
@import Foundation;

#import "PullRequest.h"
#import <Mantle/Mantle.h>
#import "Owner.h"

@interface Repository : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSNumber * id_n;
@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * full_name;
@property (nonatomic, copy) NSString * descript;
@property (nonatomic, copy) NSNumber * forks;
@property (nonatomic, copy) NSNumber * stargazers_count;
@property (nonatomic, copy) Owner * owner;
@property (nonatomic, copy) NSString *pulls_url;
@property (nonatomic, retain) NSMutableArray<PullRequest *> * pullrequests;

@end
