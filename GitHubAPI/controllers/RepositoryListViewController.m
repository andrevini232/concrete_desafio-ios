//
//  RepositoryListViewController.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "RepositoryListViewController.h"

@interface RepositoryListViewController() <RepositoryListDelegate, RepositoryPullRequestDelegate>

@end

@interface RepositoryListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) RepositoryListDelegateAndDatasource *dataSource;
@property (nonatomic) GitHuBusiness *business;

@end

@implementation RepositoryListViewController

#pragma mark - Life cicle methods
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [RepositoryListDelegateAndDatasource new];
    [self.dataSource initRepositoryListDataSourceWithDelegate:self delegatePullRequest:self];
    self.tableView.dataSource = self.dataSource;
    self.tableView.delegate = self.dataSource;
    self.tableView.pagingEnabled = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    
}

#pragma mark - Override methods
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

@implementation RepositoryListViewController (RepositoryListDelegate)

#pragma mark - RepositoryListDelegate methods

- (void)reloadFetchDataWithRepository{
    [self.tableView reloadData];
}
@end

@implementation RepositoryListViewController (RepositoryPullRequestDelegate)

#pragma mark - RepositoryPullRequestDelegate methods

- (void)pushPullRequestViewController:(Repository *)repository{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:kStoryboardID bundle: nil];
    PullRequestViewController *viewController = (PullRequestViewController *) [mainStoryboard instantiateViewControllerWithIdentifier:kPullRequestControllerID];
    [self presentViewController:viewController animated:YES completion:nil];
    [viewController initWithRepository:repository];
}

@end
