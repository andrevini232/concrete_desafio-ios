//
//  PullRequestWebViewController.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 02/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import UIKit

class PullRequestWebViewController: UIViewController {

    @IBOutlet var pullrequestWebView: PullRequestWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func setWebView(url : String){
        self.pullrequestWebView.webView.loadRequest(NSURLRequest(url: NSURL(string:url)! as URL) as URLRequest);
    }
    
}
