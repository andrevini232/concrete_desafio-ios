//
//  PullRequestViewController.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

@import UIKit;
#import "Repository.h"
#import "PullRequestDelegateAndDataSource.h"

@interface PullRequestViewController : UIViewController

- (void)initWithRepository:(Repository *)repository;

@end
