//
//  PullRequestViewController.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

#import "PullRequestViewController.h"
#import "GitHubAPI-Swift.h"
#import "UILabel+FormattedText.h"

@interface PullRequestViewController ()<RepositoryPullRequestDelegate>
@property (nonatomic) PullRequestDelegateAndDataSource *dataSource;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet PullRequestHeaderView *pullRequestHeaderView;
@end

@implementation PullRequestViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Public Methods
- (void)initWithRepository:(Repository *)repository{
    self.dataSource = [PullRequestDelegateAndDataSource new];
    [self.dataSource initPullRequestDataSourceWithRepository:repository delegate:self];
    self.tableView.delegate = self.dataSource;
    self.tableView.dataSource = self.dataSource;
    self.pullRequestHeaderView.repositoryHeaderName.text = repository.name;
}

#pragma mark - IBAction methods
- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
@implementation PullRequestViewController (RepositoryPullRequestDelegate)
#pragma mark - RepositoryPullRequestDelegate methods

- (void)setHeaderFooterMessage:(NSString *)message{
    
    self.pullRequestHeaderView.headerFooterLabel.text = message;
    [self.pullRequestHeaderView.headerFooterLabel setTextColor:[UIColor redColor] range:NSMakeRange(0, 9)];
    
}
- (void)reloadFetchDataWithPullRequests{
    
    [self.tableView reloadData];
    [self.tableView layoutIfNeeded];
}

@end
