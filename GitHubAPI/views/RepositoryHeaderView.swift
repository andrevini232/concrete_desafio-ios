//
//  RepositoryHeaderView.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 02/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import UIKit

class RepositoryHeaderView: UIView {

    @IBOutlet weak var repositoryName: UILabel!

}
