//
//  RepositoryCellTableViewCell.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//
@import UIKit;
#import "StringUtils.h"

@interface RepositoryCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *repositoryName;
@property (weak, nonatomic) IBOutlet UILabel *descriptRepository;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userFullName;
@property (weak, nonatomic) IBOutlet UILabel *forks;
@property (weak, nonatomic) IBOutlet UILabel *stars;

+ (NSString *)reuseIdentifier;
@end
