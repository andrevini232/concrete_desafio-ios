//
//  PullRequestCellTableViewCell.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

#import "PullRequestCellTableViewCell.h"

@implementation PullRequestCellTableViewCell

#pragma mark - Override methods
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


@end
