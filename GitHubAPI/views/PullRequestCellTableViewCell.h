//
//  PullRequestCellTableViewCell.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullRequestCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptPullRequest;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userLogin;
@property (weak, nonatomic) IBOutlet UILabel *userFullName;
@property (weak, nonatomic) IBOutlet UILabel *pullRequestTitle;
@property (weak, nonatomic) IBOutlet UILabel *datapullRequest;

@end
