//
//  PullRequestHeaderView.swift
//  GitHubAPI
//
//  Created by Andre Nogueira on 02/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

import UIKit

public class PullRequestHeaderView: UIView {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var repositoryHeaderName: UILabel!
    @IBOutlet weak var headerFooterLabel: UILabel!
    
}
