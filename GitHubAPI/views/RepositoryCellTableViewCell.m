//
//  RepositoryCellTableViewCell.m
//  GitHubAPI
//
//  Created by Andre Nogueira on 26/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

#import "RepositoryCellTableViewCell.h"

@implementation RepositoryCellTableViewCell

#pragma mark - Override methods
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

+ (NSString *)reuseIdentifier {
    return kPullRequestCellID;
}

@end
