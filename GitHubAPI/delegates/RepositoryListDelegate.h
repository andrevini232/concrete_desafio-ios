//
//  RepositoryListDelegate.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 31/12/17.
//  Copyright © 2017 Andre Nogueira. All rights reserved.
//

@import Foundation;

@protocol RepositoryListDelegate <NSObject>

- (void)reloadFetchDataWithRepository;
- (void)reloadFetchDataWithUserName;

@end
