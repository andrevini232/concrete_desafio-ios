//
//  RepositoryPullRequestDelegate.h
//  GitHubAPI
//
//  Created by Andre Nogueira on 01/01/18.
//  Copyright © 2018 Andre Nogueira. All rights reserved.
//

@import Foundation;
@protocol RepositoryPullRequestDelegate <NSObject>

- (void)pushPullRequestViewController:(Repository *)repository;
- (void)reloadFetchDataWithPullRequests;
- (void)setHeaderFooterMessage:(NSString *)message;

@end
